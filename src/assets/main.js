$(document).ready(function() {
  $(".slider").each(function() {
    var $this = $(this);
    var $group = $this.find(".slide_group");
    var $slides = $this.find(".slide");
    var bulletArray = [];
    var currentIndex = 0;
    var timeout;

    function move(newIndex) {
      var animateLeft, slideLeft;

      advance();

      if ($group.is(":animated") || currentIndex === newIndex) {
        return;
      }

      bulletArray[currentIndex].removeClass("active");
      bulletArray[newIndex].addClass("active");

      if (newIndex > currentIndex) {
        slideLeft = "100%";
        animateLeft = "-100%";
      } else {
        slideLeft = "-100%";
        animateLeft = "100%";
      }

      $slides.eq(newIndex).css({
        display: "block",
        left: slideLeft
      });
      $group.animate(
        {
          left: animateLeft
        },
        function() {
          $slides.eq(currentIndex).css({
            display: "none"
          });
          $slides.eq(newIndex).css({
            left: 0
          });
          $group.css({
            left: 0
          });
          currentIndex = newIndex;
        }
      );
    }

    function advance() {
      clearTimeout(timeout);
      timeout = setTimeout(function() {
        if (currentIndex < $slides.length - 1) {
          move(currentIndex + 1);
        } else {
          move(0);
        }
      }, 4000);
    }

    $(".next_btn").on("click", function() {
      if (currentIndex < $slides.length - 1) {
        move(currentIndex + 1);
      } else {
        move(0);
      }
    });

    $(".previous_btn").on("click", function() {
      if (currentIndex !== 0) {
        move(currentIndex - 1);
      } else {
        move(3);
      }
    });

    $.each($slides, function(index) {
      var $button = $('<a class="slide_btn">&bull;</a>');

      if (index === currentIndex) {
        $button.addClass("active");
      }
      $button
        .on("click", function() {
          move(index);
        })
        .appendTo(".slide_buttons");
      bulletArray.push($button);
    });

    advance();
  });

  // selected elements
  var navTrigger = document.getElementById("nav-trigger");
  var nav = document.getElementById("nav");
  var header = document.getElementById("header");
  var heading = document.getElementById("heading");
  var labels = document.getElementsByClassName("nav-label");

  // sizing
  var windowHeight = window.innerHeight;
  var windowWidth = window.innerWidth;
  var fontSize = windowHeight * 0.1;
  var headingSize = windowWidth * 0.1;

  // Event Listening
  navTrigger.addEventListener("click", navToggle);
  window.addEventListener("resize", resize);

  function resize() {
    windowHeight = window.innerHeight;
    windowWidth = window.innerWidth;
    fontSize = windowHeight * 0.1;
    headingSize = windowWidth * 0.1;
    if (headingSize > windowHeight * 0.3) headingSize = windowHeight * 0.3;

    for (var i = 0; i < labels.length; i++) {
      labels[i].style.fontSize = fontSize + "px";
      labels[i].style.height = fontSize + "px";
      labels[i].style.marginTop = "-" + fontSize * 0.6 + "px";
    }

    // header.style.height = windowHeight + "px";
    // heading.style.fontSize = headingSize + "px";
    // heading.style.height = headingSize + "px";
    // heading.style.marginTop = "-" + headingSize * 0.6 + "px";
  }

  function navToggle(e) {
    var closed = navTrigger.className.indexOf("close") > 0;
    if (closed) {
      navTrigger.className = "nav-trigger open";
      nav.className = "out";
    } else {
      navTrigger.className = "nav-trigger close";
      nav.className = "in";
    }
  }
  window.onload = resize;
  $("#nav-trigger").click(function() {
    $(".lists").toggleClass("block");
    $(".logo").toggleClass("shot");
  });
  $("#nav li a").click(function() {
    $("#nav-trigger").click();
  });
});
jQuery(document).ready(function() {
  $(".d_btn").click(function() {
    alert("click!");
  });
});
//Фиксация навигации
// $(window).scroll(function(){
//   var docscroll=$(document).scrollTop();
//   if(docscroll>$(window).height()){
//     $('.d_nav').css({'height': $('.d_nav').height(),'width': $('.d_nav').width()}).addClass('fixed');
//   }else{
//     $('.d_nav').removeClass('fixed');
//   }
// });


jQuery( document ).ready( function() {
  $("a.slider_order").click(function () {
        alert('click!');
    });
} )
