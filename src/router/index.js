import Vue from 'vue'
import Router from 'vue-router'
import About from '@/views/About'
import Home from '@/views/Home'
import Services from '@/views/Services';
import Work from '@/views/Work';
import ListWork from '@/views/ListWork';




Vue.use(Router)

export default new Router({
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About,
    },
    {
      path: '/services',
      name: 'services',
      component: Services,
   },
    {
      path: '/works',
      name: 'works.index',
      component: Work,
    },
    {
      path: '/listwork/:id',
      name: 'listwork',
      component: ListWork,
      props: true
    },
    // { path: '/404', component: NotFound },
    // { path: '*', redirect: '/404' },
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})
 